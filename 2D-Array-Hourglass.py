#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the hourglassSum function below.
def hourglassSum(arr):
    maxHourglass = -63
    
    for i in range(0,4):
        for j in range(0,4):
            singleHourglass = list()
            singleHourglass += arr[i][j:j+3]
            singleHourglass.append(arr[i+1][j+1])
            singleHourglass += arr[i+2][j:j+3]
            sumHourglass = sum(singleHourglass)
            maxHourglass = max(maxHourglass, sumHourglass)
            
    return maxHourglass
        
if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    arr = []

    for _ in range(6):
        arr.append(list(map(int, input().rstrip().split())))

    result = hourglassSum(arr)

    fptr.write(str(result) + '\n')

    fptr.close()
