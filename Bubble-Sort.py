#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the countSwaps function below.
def countSwaps(a):
    swaps = 0   
    
    for i in range(0,len(a)-1):
        for j in range(0,len(a)-1-i):
            if a[j] > a[j+1]:
                swaps += 1
                a[j], a[j+1] = a[j+1], a[j]
    first = a[0]
    last = a[-1]
    
    print("Array is sorted in " + str(swaps) + " swaps.")
    print("First Element: " + str(first))
    print("Last Element: " + str(last))
    return a
    
if __name__ == '__main__':
    n = int(input())

    a = list(map(int, input().rstrip().split()))

    countSwaps(a)
