'''
class Node:
      def __init__(self,info): 
          self.info = info  
          self.left = None  
          self.right = None 
           

       // this is a node of the tree , which contains info as data, left , right
'''

def lca(root, v1, v2):
    if v1 > v2:
        bigV, smallV = v1, v2
    else:
        bigV, smallV = v2, v1
        
    lca = root
    while True:
        if lca.info > bigV:
            lca = lca.left
        elif lca.info < smallV:
            lca = lca.right
        elif lca.info >= smallV and lca.info <= bigV:
            return lca
    
